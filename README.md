# Artixlinux Docker Steamcmd and Game Servers

This repo contains the source files to generate the steamcmd related images in [Artixlinux Docker Hub](https://hub.docker.com/u/artixlinux)

## Dependencies

It will vary depending on the image but absolutely needed packages would be:

* make
* docker

## Usage

Clone the repository and cd into the desired project read the README.md file on it and follow the instructions

## Purpose

* Provide Artix Linux based steamcmd ready to use in a Docker image
* Provide the most simple base steamcmd image for users to dockerize their steam game servers
* Provide some commonly used game servers for games that run natively under Linux
