# Artix Docker Do Not Starve Together Image

This is a ready to use docker image for Do Not Starve Together game server by [klei](https://www.klei.com/)

## Obtaining

You can obtain this image from Artix Linux docker hub or clone this repository and build the image yourself.

### Pulling from Docker Hub

To get the image already built from Docker Hub just pull it using the docker client

```
$ docker pull artixlinux/steamcmd/dont_starve_together
```

### Building the Image yourself

First clone this repository, you will need git to do so:

```
$ git clone https://gitea.artixlinux.org/artix/artixlinux-docker-steamcmd.git
$ cd docker-steamcmd/game_servers/dont_starve_together
$ make
```

This will build a new image, after completion it will be available in your local repository as `artixlinux/steamcmd_dont_starve_together`


## Configuration

In order to make our server to work you need a cluster token and some configuration files. In order to provide our own files we will be adding some docker volumes that the container will mount, for this example we are gonna use `~/.klei/cluster/config` as directory to store our configuration files and token but you can choose any other.

### Create the configuration files

First we need to create the configuration directory in the host machine, we will put our files on it

```
$ mkdir -p ~/.klei/cluster/config/{Caves,Master}
$ cd ~/.klei/cluster/config
```

Now lets get the template *ini* files from the image itself:

```
$ docker run -v $PWD:/tmp/mount --rm --entrypoint cp artixlinux/steamcmd_dont_starve_together /home/steam/.klei/DoNotStarveTogether/GameWorld/cluster.ini /tmp/mount/
$ docker run -v $PWD/Caves:/tmp/mount --rm --entrypoint cp artixlinux/steamcmd_dont_starve_together /home/steam/.klei/DoNotStarveTogether/GameWorld/Caves/server.ini /tmp/mount/
$ docker run -v $PWD/Caves:/tmp/mount --rm --entrypoint cp artixlinux/steamcmd_dont_starve_together /home/steam/.klei/DoNotStarveTogether/GameWorld/Caves/worldgenoverride.lua /tmp/mount/
$ docker run -v $PWD/Master:/tmp/mount --rm --entrypoint cp artixlinux/steamcmd_dont_starve_together /home/steam/.klei/DoNotStarveTogether/GameWorld/Master/server.ini /tmp/mount/
```

The commands above copy the relevant files from inside the docker image and put them in the right directories in our local path. Edit the
configuration files to your taste and copy your token into `cluster_token.txt` in the (in our example) `~/.klei/cluster/config` directory.

Now you can start the server running the command below from the terminal:

```
docker run \
        -v $PWD/cluster_token.txt:/home/steam/.klei/DoNotStarveTogether/GameWorld/cluster_token.txt \
        -v $PWD/cluster.ini:/home/steam/.klei/DoNotStarveTogether/GameWorld/cluster.ini \
        -v $PWD/Caves/server.ini:/home/steam/.klei/DoNotStarveTogether/GameWorl/Caves/server.ini \
        -v $PWD/Master/server.ini:/home/steam/.klei/DoNotStarveTogether/GameWorld/Master/server.ini \
        -v $PWD/Caves/worldenoverride.lua:/home/steam/.klei/DoNotStarveTogether/GameWorld/Caves/worldoverride.lua \
        --name artix_dfs artixlinux/steamcmd_dont_starve_together
```

