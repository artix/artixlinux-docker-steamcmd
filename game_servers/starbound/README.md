# Artix Docker Starbound Image

This is a ready to build docker image for Starbound game server by [Chuclefish Ltd.](https://chucklefish.org/)

## Obtaining

The only way to obtain this image is by cloning this repository and building it yourself, this is due the imposition of having to use an authenticated steam user in order to obtain the game server.

### Building the Image

First clone this repository, you will need git to do so:

```
$ git clone https://gitea.artixlinux.org/artix/artixlinux-docker-steamcmd.git
$ cd artixlinux-docker-steamcmd/game_servers/starbound
$ STEAM_USER="your_username" STEAM_PWD="your_password" make
```

> **important**: if you use Steam Guard you will need to disable it in order to build this image, after the image is built, you can then re-enable Steam Guard and then run the container with `-it` as you will get prompted to input your two factor authentication code once

This will build a new image using your **username** and **password**, after completion it will be available in your local repository as `artixlinux/steamcmd_starbound`

## Configuration

You can override default configurations using volumes and passing some variables to the `docker run` command in order to make the server to point to your own copy of the configuration files in the host disk. For this example we are gonna use `~/.starbound/config` as directory to store our configuration files and logs but you can choose any other.

### Create the configuration files

First we need to create the configuration directory in the host machine, we will put our files on it

```
$ mkdir -p ~/.starbound/{config,logs}
$ cd ~/.starbound
```

Now lets get the configuration files from the image itself:

```
$ docker run -v $PWD:/tmp/mount --rm --entrypoint cp artixlinux/steamcmd_starbound /home/steam/starbound/storage/starbound_server.config /tmp/mount/config/
```

The command above copy the server configuration file into (in our example) `~/.starbound/config/starbound_server.config` so we can make the changes that we need on it and then share it with the container using a volume.

```
docker run \
    -v $PWD/config/starbound_server.config:/home/steam/starbound/storage/starbound_server.config \
    -v $PWD/logs:/home/steam/starbound/server_logs \
    -e STARBOUND_LOG_FILE=/home/steam/starbound/server_logs/starbound_server.log \
    --name artix_starbound artixlinux/steamcmd_starbound 
```

## Other Configuration Options

There are other configuration options that one can setup in the container ar runtime:

* STARBOUND_BOOT_CONFIG: setup the path to the `-bootconfig` argument in the server
* STARBOUND_RUNTIME_CONFIG: setup the path to the `-runtimeconfig` argument in the server
* STARBOUND_LOG_LEVEL: sets the `loglevel` argument in the server
