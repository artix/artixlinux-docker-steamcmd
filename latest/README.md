# Artixlinux SteamCMD

This image is intended to be used as base image for other images designed to enable and configure game servers but it can also be used standalone if for example the users wants to manage more than one game server with a single docker container. The image can be used standalone using the following example command:

```
$ docker run -it --name=steamcmd --volume /home/user/game_servers:/home/steam/game_servers artixlinux/steamcmd bash
```

# steamcmd.sh

The `steamcmd.sh` command line utility tool can be found in the `/home/steam/steamcmd` directory

# Building from sources

Just clone this repo and execute make in the `steamcmd` directory

# Advanced Usage

You can use this docker image to install and/or update your Steam servers without needing to install SteamCMD in your personal computer or server. 

## Install/Update a Game Server

Depending on our account configuration we will be able to install and update game servers using different approach, if the user account has Steam Guard enabled it will take a bit more of effort to use the image successfully but it can be accomplished without breaking to sweat.

### When Steam Guard is enabled

If when you login into your Steam account you get asked by a code sent to your email, that means that Steam Guard is enabled in your account, if that is not the case, please skip to the **When Steam Guard is not enabled** section.

For this setup we need follow a couple of steps as we need to authenticate the docker container itself into steam, in this setup we don't delete the docker container as we need the credential files that SteamCMD stores on it to be able to install and update our applications.

#### Step 1: Start a new container

First we need to start out SteamCMD container, make sure the `--rm` flag **is not** set in the run command or this will not work, also change the *\<user\>* and *\<game_name\>* tags with your own real data:

```
$ docker run -it --name=<game_name> --volume /home/<user>/<game_name>/storage:/home/steam/<game_name> steamcmd /home/steam/steamcmd/steamcmd.sh
```

The command above will create a new docker container called *\<game_name\>* mounting a local volume located at path `/home/<user>/<game_name>/storage` into the container `/home/steam/<game_name>` path and start a new interactive `steamcmd.sh` console.

> **note**: users can choose to use another volume paths depending on their plans and setup

In the local terminal execute the following commands:

```
Steam> login <username> <password>
```

If there are no typos in the username and password you will be prompted with the following messages:

```
Logging in user '<steam_user>' to Steam Public ...

This computer has not been authenticated for your account using Steam Guard.
Please check your email for the message from Steam, and enter the Steam Guard
 code from that message.
You can also enter this code at any time using 'set_steam_guard_code'
 at the console.
Steam Guard code:
```

Go to the email account that Steam is linked to, you should find there a new email from Steam with a code, copy it and paste it in the terminal, now your container is authenticated in Steam, write `quit` in the console, that will end the SteamCMD session and return you to the terminal. 

#### Step 2: Run the newly created container

Now we need to run the newly created container in detached mode so it will be running in the background until we tell it to stop, in order to do so just type:

```
$ docker run <game_name>
```

#### Step 3: Issue commands to the container

Now we can issue commands using the running container that is already authenticated with in Steam (note that we still need to provide username and password), run the following in the terminal to install our game (remember to change \<game_name\>, \<steam_user\>, \<steam_password\> and \<game_id\> with your own valid parameters):

```
$ docker exec -it <game_name> /home/steam/steamcmd/steamcmd.sh \+login <steam_user> <steam_password> +force_install_dir /home/steam/game_name +app_update  <game_id> validate +quit
```

The command above will download and/or update the GameID we set and store it in the volume we used for it when we created the container. You can now save the command above as an script or an alias and use it every time you need to update your game server (note that the <game_name> container must be up and running to update it)

### When Steam Guard is not enabled

This is way more easier than the approach above, in case that you are using this method, we recommend you to create a separate Steam account for this as it will not be protected with 2FA.

#### Step 1 (and unique): Install or update

For this setup we will create a container that will install/update our game and be immediately destroyed after as we do not need it anymore. To do so just execute the command below in a terminal (remember to change the \<username\>, \<game_name\>, \<steam_user\>, \<steam_password\> and \<game_id\> tags with your own valid data):

```
$ docker run --rm --it --name=steamcmd --volume /home/<username>/<game_name>:/home/steam/<game_name> /home/steam/steamcmd/steamcmd.sh +login <steam_user> <steam_password> +force_install_dir /home/steam/<game_name> +app_update <game_id> validate +quit
```

That will install and/or update your Steam game server in the given volume path, now you can just save the command as an script or an alias and use it every time you need to update or install a game server

# Notes

 The `latest` image can not run pacman, in case you need to install some package you can use the `rooted` image instead but make sure to switch to `steam` user in your `Dockerfile` as user root can not run `steamcmd.sh`

SteamCMD and Steam are registered trademarks of [Valve Corporation](https://www.valvesoftware.com/en/), Artix Linux is not affiliated in any way with Valve Software or any of its products and/or partners
